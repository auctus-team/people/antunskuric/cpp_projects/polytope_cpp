import numpy as np
import itertools


def hyper_plane_shift_method(A, x_min, x_max, tol=1e-15):
    """
    Hyper plane shifting method implementation used to solve problems of a form:

    .. math:: y = Ax
    .. math:: x_min <= x <= x_max


    Note:
        *Gouttefarde M., Krut S. (2010) Characterization of Parallel Manipulator Available Wrench Set Facets. In: Lenarcic J., Stanisic M. (eds) Advances in Robot Kinematics: Motion in Man and Machine. Springer, Dordrecht*


    This algorithm can be used to calcualte acceleration polytope, velocity polytoe and even 
    polytope of the joint achievable joint torques based on the muscle forces

    Args:
        A: projection matrix
        x_min: minimal values
        x_max: maximal values 
        
    Returns
    --------
        H(list):  
            matrix of half-space representation `Hx<d`
        d(list): 
            vector of half-space representation `Hx<d`
    """

    H = []
    d = []

    x_min = np.array(x_min).reshape((-1, 1))
    x_max = np.array(x_max).reshape((-1, 1))
    A = np.array(A)
    # Combination of n x n-1 columns of A
    #C = nchoosek(1:size(A,2),size(A,1)-1)
    # print(list(itertools.combinations(range(A.shape[1]),A.shape[0]-1)))
    C = np.array(
        list(itertools.combinations(range(A.shape[1]), A.shape[0] - 1)))
    for comb in C:
        W = A[:, comb]
        U, S, V = np.linalg.svd(W.T)
        if (A.shape[0] - np.linalg.matrix_rank(W)) == 1:
            c = V[-1, :]

            # Check for redundant constraint
            if len(H):
                #diff = min(vecnorm(H - c,2,2))
                diff = np.min(np.linalg.norm(H - c, axis=1))
                if diff < tol:
                    c_exists = True
                else:
                    c_exists = False
            else:
                c_exists = False

            # Compute offsets
            if not c_exists:
                I = c.dot(A)
                I_positive = np.where(I > 0)[0]
                I_negative = np.where(I < 0)[0]
                d_positive = I[I_positive].dot(
                    np.max([x_min[I_positive], x_max[I_positive]],
                           0)) + I[I_negative].dot(
                               np.min([x_min[I_negative], x_max[I_negative]],
                                      0))
                d_negative = -I[I_negative].dot(
                    np.max([x_min[I_negative], x_max[I_negative]],
                           0)) - I[I_positive].dot(
                               np.min([x_min[I_positive], x_max[I_positive]],
                                      0))
                # Append constraints
                if len(H):
                    H = np.vstack((H, c))
                    d = np.vstack((d, [d_positive, d_negative]))
                else:
                    H = c
                    d = np.array([d_positive, d_negative])
                H = np.vstack((H, -c))

    return H, d


L = 5
n = 3
B = [[1, 2, 3, 4, -5], [4, -5, 6, -7, 8], [7, 8, 9, -10, 11]]
y_min = np.zeros(L)
y_max = np.ones(L)

# x = By
# s.t. y in [y_min, y_max]
print(hyper_plane_shift_method(B, y_min, y_max))
