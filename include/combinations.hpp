#include <iostream>
#include <stdio.h>
#include <vector>

/**
 The main function that finds all the combinations of size r
 out of n elements without repetitions and stores them to the combination vector

 @param combinations - return (vector of vectors) containing combinations
 @param n - number of elements
 @param r - length of combination
**/
void CombinationRepetition(std::vector<std::vector<int>> &combinations, int n,
                           int r);