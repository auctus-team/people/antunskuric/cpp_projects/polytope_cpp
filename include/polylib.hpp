#include "combinations.hpp"
#include <Eigen/Dense>
/**
Hyper plane shifting method implementation used to solve problems of a form:

y = A.x
x_min <= x <= x_max

The algorihms returns the half-space reperesentation of the polytope:
H.y < d

Paper describing the method
Characterization of Parallel Manipulator Available Wrench Set Facets.
by Gouttefarde M. and Krut S. (2010)

@param H  - matrix that will contain matrix H of the solution - Hy<d
@param d  - vector that will contain vector d of the solution - Hy<d
@param A  - matrix A
@param x_min - lower bound on x
@param x_max - upper bound on x

*/
void hyper_plane_shift_method(Eigen::MatrixXd &H, Eigen::VectorXd &d,
                              Eigen::MatrixXd A, Eigen::VectorXd x_min,
                              Eigen::VectorXd x_max);
