#include "combinations.hpp"
#include <bits/stdc++.h>
#include <iostream>
#include <stdio.h>

/**
    @param combinations - std vector of combinations
    @param chosen       - tmp vector conatining the currenlty constructed
combination
    @param r            - number of elements per combination
    @param start        - index to start with
    @param end          - ending index
**/
void CombinationRepetitionUtil(std::vector<std::vector<int>> &combinations,
                               std::vector<int> &chosen, int r, int start,
                               int end) {
  // when the number of choosen indexes is equal to the
  // combinaiton length
  if ((int)chosen.size() == r) {
    combinations.push_back(chosen);
    return;
  }

  // recursive choosing of all the combinations elements
  for (int i = start; i <= end; i++) {
    if (std::find(chosen.begin(), chosen.end(), i) != chosen.end()) {
      // make sure that there is no repetitions
      continue;
    }

    chosen.push_back(i);
    // choose the next element
    CombinationRepetitionUtil(combinations, chosen, r, i, end);
    chosen.pop_back();
  }
  return;
}

// The main function that finds all the combinations of size r
// out of n elements without repetitions
void CombinationRepetition(std::vector<std::vector<int>> &combinations, int n,
                           int r) {
  // Allocate memory
  std::vector<int> chosen;
  // Call the recursive function
  CombinationRepetitionUtil(combinations, chosen, r, 0, n - 1);
}
