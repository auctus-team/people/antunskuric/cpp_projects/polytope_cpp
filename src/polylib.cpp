#include "polylib.hpp"
/**
Hyper plane shifting method implementation used to solve problems of a form:

y = A.x
x_min <= x <= x_max

The algorihms returns the half-space reperesentation of the polytope:
H.y < d

Paper describing the method
Characterization of Parallel Manipulator Available Wrench Set Facets.
by Gouttefarde M. and Krut S. (2010)

@param H  - matrix that will contain matrix H of the solution - Hy<d
@param d  - vector that will contain vector d of the solution - Hy<d
@param A  - matrix A
@param x_min - lower bound on x
@param x_max - upper bound on x

*/
void hyper_plane_shift_method(Eigen::MatrixXd &H, Eigen::VectorXd &d,
                              Eigen::MatrixXd A, Eigen::VectorXd x_min,
                              Eigen::VectorXd x_max) {

  // shape(A) = (m,n)
  // find all combinations of m-1 out of n indexes
  std::vector<std::vector<int>> combinations;
  CombinationRepetition(combinations, A.cols(), A.rows() - 1);

  // for all combinations of indexes
  for (auto const &comb : combinations) {
    // construct the matrix containing the combination of rows
    // with indexes given by the combination

    //////////////////////////////////////////////////////////////
    // Eigen::MatrixXd W = A(Eigen::all, comb); // TODO: Eigen 3.4
    Eigen::MatrixXd W(A.rows(), comb.size());
    for (unsigned int i = 0; i < comb.size(); ++i) {
      W.col(i) = A.col(comb[i]);
    }
    //////////////////////////////////////////////////////////////

    // calculate the svd of this matrix
    // TODO: JacobiSVD seems ~10us faster than BDCSVD
    // it is also expected to be better for small matrices (like jacobians)
    // https://eigen.tuxfamily.org/dox/group__SVD__Module.html
    // also, it seems to fix an issue when compiling in a conda env
    // Eigen::BDCSVD<Eigen::MatrixXd> svd(W, Eigen::ComputeFullU);
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(W, Eigen::ComputeFullU);

    // find the null space projection matrix
    // (should be V.T but here is U.T - TODO find why)
    Eigen::MatrixXd V = svd.matrixU().transpose();

    // use singular values to determine if the matrix W is singular
    Eigen::VectorXd s = svd.singularValues();
    // if (s(Eigen::last) != 0) { // TODO: Eigen 3.4
    if (s(s.size() - 1) != 0) {

      // use the last vector of the null space projector
      // as the normal of the half-space

      // Eigen::VectorXd c = V(Eigen::last, Eigen::all); // TODO: Eigen 3.4
      Eigen::VectorXd c = V.row(V.rows() - 1);

      // check if this normal has allready been added to H
      if (H.size()) {
        if ((H.rowwise() - c.transpose()).colwise().norm().minCoeff() != 0) {
          // not added yet so add it
          H.conservativeResize(H.rows() + 2, H.cols());
          H.row(H.rows() - 2) = c;
          H.row(H.rows() - 1) = -c;
        } else {
          // already added this normal
          continue;
        }
      } else {
        // first time adding a normal
        H = c.transpose();
        H.conservativeResize(H.rows() + 1, H.cols());
        H.row(H.rows() - 1) = -c;
      }

      // Calculate maximal distances of the half-planes
      // with normal vecotr c form the origin
      Eigen::VectorXd I = c.transpose() * A;
      double d_positive(0.0f), d_negative(0.0f);
      // to find the max you can sum only positive values
      // and min only negative ones
      for (int i = 0; i < I.size(); i++) {
        if (I[i] > 0) {
          d_positive += I[i] * x_max[i];
          d_negative -= +I[i] * x_min[i];
        } else {
          d_positive += I[i] * x_min[i];
          d_negative -= I[i] * x_max[i];
        }
      }
      // add the distances to the vector d
      d.conservativeResize(d.size() + 2);
      d(d.size() - 2) = d_positive;
      d(d.size() - 1) = d_negative;
    }
  }
}
