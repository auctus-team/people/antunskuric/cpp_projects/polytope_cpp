#include <Eigen/Core>
#include <Eigen/Dense>
#include <chrono>
#include <tuple>
#include <vector>
// #include <iostream>

#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "polylib.hpp"

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

using Vector6d = Eigen::Matrix<double, 6, 1>;
using RefVector6d = const Eigen::Ref<Vector6d>;

struct HalfPlane {
  Eigen::MatrixXd H;
  Eigen::VectorXd d;
};

template <typename T> struct ResTime {
  void tick() { startt = std::chrono::steady_clock::now(); }
  void tock() {
    endd = std::chrono::steady_clock::now();
    total_ns =
        std::chrono::duration_cast<std::chrono::nanoseconds>(endd - startt)
            .count();
  }

  double total_ns;
  std::chrono::time_point<std::chrono::steady_clock> startt;
  std::chrono::time_point<std::chrono::steady_clock> endd;
  T r;
};

using ResTimeVector = ResTime<Vector6d>;
using ResTimeHalfPlane = ResTime<HalfPlane>;

ResTimeVector test_vec(std::vector<Vector6d> vec) {

  ResTimeVector restime;
  restime.r.setZero();
  restime.tick();
  for (const auto &elem : vec) {
    restime.r += elem;
  }
  restime.tock();
  return restime;
}

ResTimeHalfPlane hyper_plane_shift_method_timed(Eigen::MatrixXd A,
                                                Eigen::VectorXd x_min,
                                                Eigen::VectorXd x_max) {

  ResTimeHalfPlane restime;
  restime.tick();
  hyper_plane_shift_method(restime.r.H, restime.r.d, A, x_min, x_max);
  restime.tock();

  return restime;
}

namespace py = pybind11;

PYBIND11_MODULE(polycap, m) {
  py::class_<ResTimeVector>(m, "ResTimeVector")
      .def(py::init<>())
      .def_readonly("total_ns", &ResTimeVector::total_ns)
      .def_readwrite("result", &ResTimeVector::r);

  py::class_<HalfPlane>(m, "HalfPlane")
      .def(py::init<>())
      .def_readwrite("H", &HalfPlane::H)
      .def_readwrite("d", &HalfPlane::d);

  py::class_<ResTimeHalfPlane>(m, "ResTimeHalfPlane")
      .def(py::init<>())
      .def_readonly("total_ns", &ResTimeHalfPlane::total_ns)
      .def_readwrite("halfplane", &ResTimeHalfPlane::r);

  m.doc() = R"pbdoc(
           Real-time capable task-space capacity calculation python module.)pbdoc";

  m.def("test_vec", &test_vec, R"pbdoc(
        test sum std::vector of Vector6d
    )pbdoc");

  m.def("hyper_plane_shift_method", &hyper_plane_shift_method_timed,
        py::arg("A"), py::arg("x_min"), py::arg("x_max"), R"pbdoc(
        The hyper plane shift method
    )pbdoc");

#ifdef VERSION_INFO
  m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
  m.attr("__version__") = "dev";
#endif
}
