# C/C++ based polytope evaluation library - polylib

At the moment the only implemented algorihtm is the *Hyper-plane shifting method* which finds the half-space representation of the polytopes 
```
P = {y | y = Ax,   x_min < x < x_max} 
```
It returns inequality constraints on the output variable `y`
```
Hy<d
```
The algorihm was proposed in the paper:

> Characterization of Parallel Manipulator Available Wrench Set Facets. *by Gouttefarde M. and Krut S. (2010)*

#### Algorithm limitations:  
Dimension of the vector `x` has to be equal or higher to the dimension of the vector `y`. And the dimension of the vector `y` has to be higher than 1.

```
dim(y) <= dim(x)
dim(y) > 1
```


## Preparing the environment

The code's only **hard dependancy is Eigen 3.4**.

The repo comes with the conda environment ready for compilation in a form of the yaml file. You can create and activate the anaconda environment by:
```sh
conda env create -f env.yaml
conda activate polylib
```

### Compile the c++ code
Compile the code using cmake
```sh
mkdir build
cd build
cmake ..
make
```

Once it is compiled you can run the example code 
```
./example
```

### Testing 
The repositiory has a python implementation of the algorihtms so you 
can it against the cpp code:
```
python example.py
```

### Testing C++-Python Wrapper
```
cd build/
pip install -e .
python ../example_cpp.py
```

### Using from cmake external project
```
cmake_minimum_required (VERSION 3.16)
project (myproject)
 
find_package(polycap REQUIRED)
 
add_executable (example example.cpp)
target_link_libraries (example polycap::polycap_cpp)
```

### Using with catkin

add to you `package.xml`

```xml
  <build_depend>polycap</build_depend>
  <exec_depend>polycap</exec_depend>
```



