# Get all target files
file(GLOB_RECURSE
     ALL_CXX_SOURCE_FILES
     RELATIVE
     "${CMAKE_SOURCE_DIR}" "*/*.cpp"
     )

file(GLOB_RECURSE
     ALL_HEADER_FILES
     RELATIVE
     "${CMAKE_SOURCE_DIR}" "include/*.hpp"
     )
message("cpps: ${ALL_CXX_SOURCE_FILES}")

# Exclude all files in 3rd dirs.
list(FILTER ALL_CXX_SOURCE_FILES EXCLUDE REGEX "3rd/*")
list(FILTER ALL_CXX_SOURCE_FILES EXCLUDE REGEX "build*")
# list(FILTER ALL_CXX_SOURCE_FILES EXCLUDE REGEX "^.*")
# list(FILTER ALL_CXX_SOURCE_FILES EXCLUDE REGEX "^/src/.")

# Find clang-format
find_program(clang_format_binpath NAMES clang-format clang-format-10 PATHS "/usr/bin/")

# Adding clang-format target if executable is found
add_custom_target(
    clang-format
    ALL
    COMMAND cd ${CMAKE_SOURCE_DIR} && ${clang_format_binpath}
    -i
    ${ALL_CXX_SOURCE_FILES}
    ${ALL_HEADER_FILES}
    )

if (${TIDY})
     message("CLANG-TIDY ENABLED")
     set(ALL_VAR ALL)
endif()

find_program(clang_tidy_binpath NAMES clang-tidy-10 clang-tidy PATHS "/usr/bin/")
add_custom_target(
     clang-tidy
     ${ALL_VAR}
     COMMAND
     cd ${CMAKE_SOURCE_DIR} && ${clang_tidy_binpath}
     ${ALL_CXX_SOURCE_FILES}
     -p ${CMAKE_BINARY_DIR}
     )
