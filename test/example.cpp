#include "polylib.hpp"
#include <Eigen/Dense>
#include <iostream>
#include <stdio.h> /* C input/output                       */
#include <vector>

int main() {

  Eigen::MatrixXd H;
  Eigen::VectorXd d;

  Eigen::MatrixXd A(3, 5);
  A << 1, 2, 3, 4, -5, 4, -5, 6, -7, 8, 7, 8, 9, -10, 11;
  Eigen::VectorXd y_max(5), y_min(5);
  y_max << 1, 1, 1, 1, 1;
  y_min = -y_max;

  hyper_plane_shift_method(H, d, A, y_min, y_max);

  std::cout << "H = " << H << std::endl;
  std::cout << "d = " << d << std::endl;
  return 0;
}
