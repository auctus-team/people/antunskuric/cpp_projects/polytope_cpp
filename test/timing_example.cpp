#include "polylib.hpp"
#include <Eigen/Dense>
#include <iostream>
#include <stdio.h> /* C input/output                       */
#include <vector>

#include <chrono>

int main() {

  Eigen::MatrixXd H;
  Eigen::VectorXd d;

  Eigen::MatrixXd A(3, 10);
  A << 1, 2, 3, 4, -5, 1, 9, 4, -21, 1, 4, -5, 6, -7, 8, 5, -91, 41, -11, 51, 7,
      8, 9, -10, 11, 2, -21, -31, -17, 8;
  Eigen::VectorXd y_max(10), y_min(10);
  y_max << 1, 1, 1, 1, 1, 1, 1, 1, 1, 1;
  y_min = -y_max;

  std::chrono::steady_clock::time_point begin =
      std::chrono::steady_clock::now();
  for (int i = 0; i < 10000; i++) {
    H = Eigen::MatrixXd();
    d = Eigen::VectorXd();
    hyper_plane_shift_method(H, d, A, y_min, y_max);
  }
  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

  std::cout << "System size : 10 input, 3 output" << std::endl;
  std::cout << "Avg exec time = "
            << std::chrono::duration_cast<std::chrono::microseconds>(end -
                                                                     begin)
                       .count() /
                   10000.0f
            << "[us]" << std::endl;
  // std::cout << "H = " << H << std::endl;
  // std::cout << "d = " << d << std::endl;
  return 0;
}