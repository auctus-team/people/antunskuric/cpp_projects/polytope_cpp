import numpy as np
import itertools
###################################################################
# build to sys.path
# TODO: this is a horrible hack
import os
import sys
import time

srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
print('abspath(__file__):', srcabspath)

buildabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                            'build')
print('buildabspath:', buildabspath)
sys.path.append(buildabspath)
import polycap


def tock_us(before):
    return (time.process_time_ns() - before) / 1e3


def tick():
    return time.process_time_ns()


def hyper_plane_shift_method(A, x_min, x_max, tol=0):
    """
    Hyper plane shifting method implementation used to solve problems of a form:

    .. math:: y = Ax
    .. math:: x_min <= x <= x_max


    Note:
        *Gouttefarde M., Krut S. (2010) Characterization of Parallel Manipulator Available Wrench Set Facets. In: Lenarcic J., Stanisic M. (eds) Advances in Robot Kinematics: Motion in Man and Machine. Springer, Dordrecht*


    This algorithm can be used to calcualte acceleration polytope, velocity polytoe and even 
    polytope of the joint achievable joint torques based on the muscle forces

    Args:
        A: projection matrix
        x_min: minimal values
        x_max: maximal values 
        
    Returns
    --------
        H(list):  
            matrix of half-space representation `Hx<d`
        d(list): 
            vector of half-space representation `Hx<d`
    """

    H = []
    d = []

    x_min = np.array(x_min).reshape((-1, 1))
    x_max = np.array(x_max).reshape((-1, 1))
    A = np.array(A)
    # Combination of n x n-1 columns of A
    #C = nchoosek(1:size(A,2),size(A,1)-1)
    # print(list(itertools.combinations(range(A.shape[1]),A.shape[0]-1)))
    C = np.array(
        list(itertools.combinations(range(A.shape[1]), A.shape[0] - 1)))
    for comb in C:
        W = A[:, comb]
        U, S, V = np.linalg.svd(W.T)
        if (A.shape[0] - np.linalg.matrix_rank(W)) == 1:
            c = V[-1, :]

            # Check for redundant constraint
            if len(H):
                #diff = min(vecnorm(H - c,2,2))
                diff = np.min(np.linalg.norm(H - c, axis=1))
                if diff < tol:
                    c_exists = True
                else:
                    c_exists = False
            else:
                c_exists = False

            # Compute offsets
            if not c_exists:
                I = c.dot(A)
                I_positive = np.where(I > 0)[0]
                I_negative = np.where(I < 0)[0]
                d_positive = I[I_positive].dot(
                    np.max([x_min[I_positive], x_max[I_positive]],
                           0)) + I[I_negative].dot(
                               np.min([x_min[I_negative], x_max[I_negative]],
                                      0))
                d_negative = -I[I_negative].dot(
                    np.max([x_min[I_negative], x_max[I_negative]],
                           0)) - I[I_positive].dot(
                               np.min([x_min[I_positive], x_max[I_positive]],
                                      0))
                # Append constraints
                if len(H):
                    H = np.vstack((H, c))
                    d = np.vstack((d, [d_positive, d_negative]))
                else:
                    H = c
                    d = np.array([d_positive, d_negative])
                H = np.vstack((H, -c))

    return H, d


L = 7
n = 6
B = np.random.rand(n, L) * 2 - 1
y_min = -np.ones(L)
y_max = np.ones(L)

# x = By
# s.t. y in [y_min, y_max]

tpy = tick()
Hpy, dpy = hyper_plane_shift_method(B, y_min, y_max)
compute_time_py = tock_us(tpy)  # [us]

res = polycap.hyper_plane_shift_method(B, y_min, y_max)
Hcpp, dcpp = res.halfplane.H, res.halfplane.d

print('Speed test:')
print('  - Python took {:.1e}us'.format(compute_time_py))
print('  - C++ took    {:.1e}us'.format(res.total_ns / 1e3))
print('Testing H, d values are the same in C++ and Python')

print('  - H test: {}'.format('OK' if np.all(
    np.allclose(np.abs(Hcpp), np.abs(Hpy), atol=1e-5)) else 'FAIL'))
print('  - d test: {}'.format(
    'OK' if np.all(np.allclose(dcpp, dpy.flatten())) else 'FAIL'))

###############################################################
# halfplane size test
print('Testing Halfplane size...')
Hrows, Hcols, drows, dcols = None, None, None, None
L = 7
n = 6
y_min = -np.ones(L)
y_max = np.ones(L)


def check(a, b, i, msg):
    if a != b:
        print('i:', i, msg)


for i in np.arange(10000):
    B = np.random.rand(n, L) * 2 - 1
    # print('i:', i, 'B:', B)
    res = polycap.hyper_plane_shift_method(B, y_min, y_max)
    H, d = res.halfplane.H, res.halfplane.d
    if Hrows is None:
        Hrows = H.shape[0]
    if Hcols is None:
        Hcols = H.shape[1]
    if drows is None:
        drows = len(d)
    if dcols is None:
        dcols = 1
    check(Hrows, H.shape[0], i, 'Hrows changed!')
    check(Hcols, H.shape[1], i, 'Hcols changed!')
    check(drows, len(d), i, 'drows changed!')
